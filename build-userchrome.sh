#!/usr/bin/env sh

grep -E -v -h '^([[:space:]]\*/?|/\*|$)' \
    ./theme/components/colors.css \
    ./theme/components/buttonbox.css \
    ./theme/components/hide-elements.css \
    ./theme/components/icons.css \
    ./theme/components/buttonbox.css \
    ./theme/components/titlebar.css \
    ./theme/components/navbar.css \
    ./theme/components/urlbar.css \
    ./theme/components/wintitle.css \
    ./theme/components/menubar.css \
    ./theme/components/colors.css \
    ./theme/components/gtktiled.css \
    ./theme/components/inactive.css > ./build/userChrome.css
